"use strict";

const eyeSlash1 = document.querySelector(".fa-eye-slash");
const eye1 = document.querySelector(".fa-eye");
const eyeSlash2 = document.querySelector(".fa-eye-slash2");
const eye2 = document.querySelector(".fa-eye2");
const input1 = document.querySelector(".input1");
const input2 = document.querySelector(".input2");
const btn = document.querySelector(".btn");

const p =document.createElement('p');
p.textContent = "Потрібно ввести однакові значення";
p.style.color = "red";
p.style.display = "none";
input1.after(p);

eyeSlash1.style.display = "none";
eyeSlash2.style.display = "none";

eyeSlash1.addEventListener("click",()=>{
    showAndHide(eye1,eyeSlash1);
    input1.type ="password";
})

eye1.addEventListener("click",()=>{
    showAndHide(eyeSlash1,eye1);
    input1.type ="text";
})

eyeSlash2.addEventListener("click",()=>{
    showAndHide(eye2,eyeSlash2);
    input2.type ="password";
})

eye2.addEventListener("click",()=>{
    showAndHide(eyeSlash2,eye2);
    input2.type ="text";
})

btn.addEventListener("click",()=>{
    if(input1.value !== input2.value){
        p.style.display = "block";
    }else{
        p.style.display = "none";
        alert("You are welcome");
    }
})


function showAndHide(show,hide){
    hide.style.display = "none";
    show.style.display = "block";
}